#include <iostream>
using namespace std;
bool is_ordered(int a[], unsigned int izq, unsigned int der){
 	bool b = true;
    for (unsigned int i = izq; i + 1 < der && b; i++)
		b = a[i] <= a[i+1];
    return b;
}

int thanos_rec(int a[], unsigned int izq, unsigned int der){
	
	if (is_ordered(a, izq,  der))
	{
		return der+1;
	}
	else {
		int med = (izq+der)/2;
		thanos_rec(a, izq, med);
		thanos_rec(a, med+1, der);
	}

} 


int main(){
	unsigned int n;
	cin >> n;
	int a[n];
	for (int i = 0; i < n; i++) {
	 	cin >> a[i];
	 }
	 cout << endl << thanos_rec(a, 0, n-1) << endl;
}