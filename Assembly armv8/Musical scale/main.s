/*
	main.s
	El código genera un efecto sonoro de la escala de notas musicales dadas en la consigna, afinadas, y con una duracion estimada de 5 		segundos aproximadamente, y ya habiendose reproducido las 7 notas, comienza nuevamente.
*/
.text
.org 0x0000

.equ PERIPHERAL_BASE, 0x3F000000 // Peripheral Base Address
.equ GPIO_BASE, 0x200000 	// GPIO Base Address
.equ GPIO_GPFSEL1, 0x4 		// GPIO Function Select 1
.equ GPIO_GPSET0, 0x1C 		// GPIO Pin Output Set 0
.equ GPIO_GPCLR0, 0x28 		// GPIO Pin Output Clear 0
.equ GPIO_FSEL8_OUT, 0x1000000 	// GPIO Function Select: GPIO Pin X8 Is An Output
.equ GPIO_18, 0x40000 		// GPIO Pin 0: 18


	// Set Cores 1..3 To Infinite Loop
	mrs X0, MPIDR_EL1 	// X0 = Multiprocessor Affinity Register (MPIDR)
	ands X0,X0,3 		// X0 = CPU ID (Bits 0..1)
	b.ne CoreLoop 		// IF (CPU ID != 0) Branch To Infinite Loop (Core ID 1..3)

	// Load in W0 the GPIO base address
	ldr X0,=(PERIPHERAL_BASE + GPIO_BASE)

	// Config GPIO18 as output
	mov W1,GPIO_FSEL8_OUT
	str W1,[X0,GPIO_GPFSEL1]

	// reg W1 contains the mask for set/clear the gpios
	mov W1,GPIO_18

.equ seg, 0x884
.equ C, 0x25c6
.equ D, 0x21aa
.equ E, 0x1dff
.equ F, 0x1c4f
.equ G, 0x193a
.equ A, 0x167b
.equ B, 0x1406

	//------------------ CODE HERE ------------------------------------------------------
infloop:

	NOP
 
DO:
	mov X9, C
	mov X10, seg
	sub X10, X10, #811
	bl make_sound

RE:
	mov X9, D
	mov X10, seg
	sub X10, X10, #712
	bl make_sound

MI:
	mov X9, E
	mov X10, seg
	sub X10, X10, #530
	bl make_sound

FA:
	mov X9, F
	mov X10, seg
	sub X10, X10, #434
	bl make_sound

SOL:
	mov X9, G
	mov X10, #1960
	bl make_sound

LA:
	mov X9, A
	mov X10, #2460
	bl make_sound

SI:
	mov X9, B
	mov X10, #2472
	bl make_sound

b infloop
	
make_sound: // Prende y apaga el parlante
	mov X4,X10
make_sound2:
	str W1,[X0,GPIO_GPSET0]	// Set GPIO18
	mov X3,X9
label_sound1:
	SUB X3,X3,#1
	CBNZ X3,label_sound1
	
    str W1,[X0,GPIO_GPCLR0]	// Clear GPIO18
	mov X3,X9
label_sound2:
	SUB X3,X3,#1
	CBNZ X3,label_sound2

	SUB X4,X4,#1
	CBNZ X4,make_sound2
	RET	
	

	//----------------------------------------------------------------------------------

CoreLoop:       // Infinite Loop For Core 1..3
  b CoreLoop

