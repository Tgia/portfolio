﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dinosaurio : MonoBehaviour
{
    public Rigidbody2D rb;
    float tecla;
    int fuerza;
    public GameObject gameOver;

    void Start()
    {
        Time.timeScale = 1;
        fuerza = 15;
    }


    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space))
        {
            rb.velocity = Vector2.up * fuerza;
            this.enabled = false;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        this.enabled = true;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Destroy(this.gameObject);
    }

    private void OnDestroy()
    {
        Time.timeScale = 0f;
        gameOver.SetActive(true);
    }

}
