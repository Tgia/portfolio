﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Generador : MonoBehaviour
{
    public GameObject[] prefabs;
    float randomTime;
    int id;
    float velocidadIncremental;
    Obstaculo obstaculo;

    void Start()
    {
        velocidadIncremental = 0.05f;
        Generar();
    }

    
    void Generar()
    {
        id = Random.Range(0, prefabs.Length);
        GameObject clon = Instantiate(prefabs[id]);
        obstaculo = clon.GetComponent<Obstaculo>();
        velocidadIncremental += 0.01f;
        obstaculo.velocidad += velocidadIncremental;
        randomTime = Random.Range(2.5f, 5f);
        Invoke("Generar", randomTime);
    }
}
