﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenerateObstacle : MonoBehaviour
{
    public GameObject cubeObj;
    Vector3 pos;
    bool next;
    public float[] posX;
    public float[] posZ;
    int value = 1;
    public int lastpos = 1;
    float randomGeneration;

    void FixedUpdate ()
    {
        next = true;
        Prefabs();
        //StartCoroutine(WaitSys());
    }
    /*IEnumerator WaitSys()
    {
        yield return new WaitForSeconds(1f);
        next = true;
        Prefabs();
    }*/
void Prefabs()
{   
        randomGeneration = Random.Range(0f, 20f);
        Invoke ("Generate", randomGeneration);
}
    
void Generate()
{
    if (!next)
    {
        return;
    }
    int i = Random.Range(0, 3);
    pos.x = posX[i];
    pos.z += posZ[i];
    GameObject cubeClon = Instantiate(cubeObj, pos, cubeObj.transform.rotation);
    cubeClon.GetComponent<CubeScript>().myNum = value;
    cubeClon.transform.SetParent(this.transform);
    value += 1;
    next = false;
    return;
}

public void Message(int i)
{
        if(lastpos == i )
        {
            lastpos += 1;
            Debug.Log("found!!");
        } else
        {
            Debug.Log("not found");
            Application.LoadLevel(0);
        }
    }
}
