﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float speed = 90f;
    public float Maxspeed = 3.5f;
    public float saltoPower = 7f;
    private bool saltoB;
    public bool tocoSuelo;
    public int engranaje = 0;
    public int Nivel = 0;
    static AudioSource audio;
    public GameObject enemy01;
    public GameObject pisoElevador;
    public GameObject PisoNivel;
    public GameObject Eng;
    public Animator animEng;

    public Rigidbody2D rb2;
    private Animator animator;
    public static PlayerController PC;
    public Animator animElevador;
    
    public AudioClip Jump1, Jump2, Recoger, NemesisHit, Palanca, RelojNW, MachineNW;
    //public static AudioClip Jump1, Jump2, Recoger, NemesisHit, Palanca, RelojNW, MachineNW;
    // Start is called before the first frame update
    void Awake()
    {
        PC = this;
        animator = GetComponent<Animator>();
        enemy01.SetActive(false);
        PisoNivel.SetActive(false);
        Eng.SetActive(false);
        /*
        Jump1 = Resources.Load<AudioClip>("jump1");
        Jump2 = Resources.Load<AudioClip>("jump2");
        Recoger = Resources.Load<AudioClip>("agarrar");
        NemesisHit = Resources.Load<AudioClip>("nemesishit");
        Palanca = Resources.Load<AudioClip>("palanca");
        RelojNW = Resources.Load<AudioClip>("relojworking");
        MachineNW = Resources.Load<AudioClip>("machinenotworking");
        */

        audio = GetComponent<AudioSource>();
        
        
    }

    public void Reproducir(AudioClip clip)
    {
        audio.PlayOneShot(clip);
        /*
        if (clip == "Jump1") audio.PlayOneShot(Jump1);
        if (clip == "Jump2") audio.PlayOneShot(Jump2);
        if (clip == "Recoger") audio.PlayOneShot(Recoger);
        if (clip == "NemesisHit") audio.PlayOneShot(NemesisHit);
        if (clip == "Palanca") audio.PlayOneShot(Palanca);
        if (clip == "RelojNW") audio.PlayOneShot(RelojNW);
        if (clip == "MachineNW") audio.PlayOneShot(MachineNW);
        */
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.UpArrow) && tocoSuelo)
        {
            saltoB = true;
            Reproducir(Jump1);
        }

        animator.SetFloat("Speed", Mathf.Abs(rb2.velocity.x));
        animator.SetBool("SobreSuelo", tocoSuelo); 
    }

    void FixedUpdate()
    {
        float h = Input.GetAxis("Horizontal");
        rb2.AddForce(Vector2.right * speed * h);

        if(rb2.velocity.x > Maxspeed)
        {
            rb2.velocity = new Vector2(Maxspeed,rb2.velocity.y);

            
        }
        if(rb2.velocity.x < -Maxspeed)
        {
            rb2.velocity = new Vector2(-Maxspeed,rb2.velocity.y);
        }
        
        if(saltoB)
        {
            rb2.AddForce(Vector2.up * saltoPower,ForceMode2D.Impulse);
            saltoB = false;
            Reproducir(Jump1);
        }  



        if(h > 0.1f)
        {
            transform.localScale = new Vector3(0.6296169f,0.6296169f,0.6296169f);
        }
        if(h < -0.1f)
        {
            transform.localScale = new Vector3(-0.6296169f,0.6296169f,0.6296169f);
        }
    }
    
    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.tag == "Engranaje")
        {
            Destroy(other.gameObject);
            engranaje = 1;
            Reproducir(Recoger);
        }
        if(other.gameObject.name == "Reloj")
        {
            Eng.SetActive(true);
            animEng.SetBool("eng", true);
        }
        if(other.gameObject.name == "Stairs01")
        {
            transform.position = new Vector3(-7.84f, -2.09f, -5f);
        }
        if(other.gameObject.name == "Stairs02")
        {
            transform.position = new Vector3(-5.5f, 0.89f, -5f);
        }
        if(other.gameObject.name == "Compuerta01")
        {
            Nivel = 1;
            Camera.main.GetComponent<moverCamara>().SetCamera();
            transform.position = new Vector3(-16.52f, -13.71f, -5f);
            enemy01.SetActive(true);
            
        }
    }
    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.name == "door01" && engranaje == 1)
        {
            Destroy(other.gameObject);
        }
        if (other.gameObject.name == "Elevador")
        {
            animElevador.SetBool("Elevador", true);
            pisoElevador.SetActive(false);
            PisoNivel.SetActive(true);
        }
        
    }

}
