﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformWH : MonoBehaviour
{
    public Animator animPlatform;
    public bool palancaWH = false;

    // Start is called before the first frame update
    void Start()
    {
        animPlatform = GetComponentInChildren<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (palancaWH == true)
        {
            animPlatform.enabled = true;
        }
        if (palancaWH == false)
        {
            animPlatform.enabled = false;
        }
    }
}
