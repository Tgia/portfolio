﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moverCamara : MonoBehaviour
{

    public Vector3 offset;
    public GameObject objetivo;
    public float desplazamiento = 0.1f;

    [SerializeField]
    float zoomFactor = 1f;

    [SerializeField]
    float zoomSpeed = 5.0f;

    private float originalSize = 0f;

    private Camera thisCamera;

    // Start is called before the first frame update
    void Start()
    {
        offset = new Vector3(0f, 0f, 0);
        objetivo = null;
        thisCamera = GetComponent<Camera>();
        originalSize = thisCamera.orthographicSize;

        
    }

    void Update()
    {
        float targetSize = originalSize * zoomFactor;
        if (targetSize != thisCamera.orthographicSize)
        {
            thisCamera.orthographicSize = Mathf.Lerp(thisCamera.orthographicSize, 
        targetSize, Time.deltaTime * zoomSpeed);
        }
        
        
    }

    public void SetCamera()
    {
        offset = new Vector3(3.2f, 2.8f, 2);
        zoomFactor = 0.8f;
        objetivo = GameObject.Find("Player");
    }
    // Update is called once per frame
    void FixedUpdate()
    {
        if(objetivo != null)
        {
            Vector3 pos = objetivo.transform.position;
            pos.z = -10;
            transform.position = Vector3.Lerp(transform.position, pos + offset, desplazamiento);
        }
        
    }

    void SetZoom(float zoomFactor)
    {
        this.zoomFactor = zoomFactor;
    }
}
