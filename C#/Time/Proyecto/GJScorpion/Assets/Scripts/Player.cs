﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public float speed = 90f;
    public float Maxspeed = 3.5f;
    public float saltoPower = 5f;
    private bool saltoB;
    public bool saltoB2;
    public bool tocoSuelo;
    public int engranaje = 0;

    public Rigidbody2D rb2;
    //private Animator animator;
    // Start is called before the first frame update
    void Start()
    {
        rb2 = GetComponent<Rigidbody2D>();
       // anim = GetComponent<Animator>();
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.UpArrow) && tocoSuelo)
        {
            saltoB = true;
            saltoB2 = true;
        }
    }

    void FixedUpdate()
    {
        float h = Input.GetAxis("Horizontal");
        rb2.AddForce(Vector2.right * speed * h);

        if(rb2.velocity.x > Maxspeed)
        {
            rb2.velocity = new Vector2(Maxspeed,rb2.velocity.y);
        }
        if(rb2.velocity.x < -Maxspeed)
        {
            rb2.velocity = new Vector2(-Maxspeed,rb2.velocity.y);
        }
        
        if(saltoB && saltoB2)
        {
            rb2.AddForce(Vector2.up * saltoPower,ForceMode2D.Impulse);
            saltoB = false;
        }  

        if(saltoB2)
        {
            rb2.AddForce(Vector2.up * (saltoPower),ForceMode2D.Impulse);
            saltoB2 = false;
        }

        if(h > 0.1f)
        {
            transform.localScale = new Vector3(0.6296169f,0.6296169f,0.6296169f);
        }
        if(h < 0.1f)
        {
            transform.localScale = new Vector3(-0.6296169f,0.6296169f,0.6296169f);
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.tag == "Engranaje")
        {
            Destroy(other.gameObject);
            engranaje = 1;
        }
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.name == "door01" && engranaje == 1)
        {
            Destroy(other.gameObject);
        }
    }

}
