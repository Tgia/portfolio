﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PalancaController : MonoBehaviour
{
    public Animator animPalanca;
    public bool palanca = false;
    private bool doorTrigger;
    public Rigidbody2D rb;
    public bool platform = false;
    public PlatformWH platWH;
    private PlayerController player;

    // Start is called before the first frame update
    void Start()
    {
        player = GetComponentInParent<PlayerController>();
        animPalanca = GetComponentInChildren<Animator>();
        rb = GameObject.Find("Player").GetComponent<PlayerController>().rb2;
        Debug.Log(platWH);
        if (this.gameObject.name == "palancaPaloL101")
        {
            Debug.Log(animPalanca.name);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space))
        {
            doorTrigger = true;
            rb.WakeUp();
        }
    }

    void OnTriggerStay2D (Collider2D other)
    {
        if(doorTrigger == true && other.gameObject.tag == "Player")
        {
            switch (palanca)
            {
                case true:
                    animPalanca.SetBool("onTouch",false);
                    palanca = false;
                    platWH.palancaWH = false;
                    PlayerController.PC.Reproducir(PlayerController.PC.Palanca);
                break;
                case false:
                    animPalanca.SetBool("onTouch",true);
                    palanca = true;
                    platWH.palancaWH = true;
                    PlayerController.PC.Reproducir(PlayerController.PC.Palanca);
                    
                break;
            }
        }
        doorTrigger = false;
    }
}
