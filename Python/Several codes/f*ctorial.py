def factorial( n ):
   if n <1:   # base case
       return 1
   else:
       returnNumber = n * factorial( n - 1 )  # recursive call
       print(str(n) + '! = ' + str(returnNumber))
       return returnNumber


def foo(*args):
     for a in args:
         print (a)
     return a
factorial(foo(1,2,3,4,5,6))