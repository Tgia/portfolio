change = [1, 'pennies', 2, 'dimes',False, 3, 'quarters', True, None , (1,2,3,4)]
def findtype(change):
    string_change =[]
    int_change = []
    bool_change = []
    none_change = []
    other_change = []
    for eachvalue in change:
        if type(eachvalue) == str:
            string_change.append(eachvalue)
        elif type(eachvalue) == int:
            int_change.append(eachvalue)
        elif type(eachvalue) == bool:
            bool_change.append(eachvalue)
        elif eachvalue is None:
            none_change.append(eachvalue)
        else:
            other_change.append(eachvalue)
 
 
    return {"string":string_change, "int" :int_change,"bol":bool_change,"none":none_change,"other":other_change}
 
o = findtype([1, True ,None , False, "Pennies", {1,2,3,4},(1,2,3,4),None,5.0])
 
for k,v in o.items() :
    print (k," : ",v )