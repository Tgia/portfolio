def arithmetic_mean_xxx(x, *l):
    """ The function calculates the arithmetic mean of a non-empty
        arbitrary number of numbers """
    sum = x
    for i in l:
        sum += i
 
    return sum / (1.0 + len(l))
  
print(arithmetic_mean_xxx(2,3))