def apply_to_list(some_list, short_function): 
    return [short_function(x) for x in some_list]

ints = [4, 0, 1, 5, 6] 
print(ints)
print(apply_to_list(ints, lambda x: (x * 2)+20))
