a = [1, 2, 3]
b=a.copy()
 
a.append(4)

def isiterable(obj):
    try:
        iter(obj)
        return True
    except TypeError: # not iterable
        return False
 
isiterable('a string')
 
isiterable([1, 2, 3])
 
iter(b)
print(isiterable(b))