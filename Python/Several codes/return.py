def add_and_maybe_multiply(a, b, c=None): 
    result = a + b
 
    if c is not None:
        result = result * c
 
    return result

print(add_and_maybe_multiply(1,2,))