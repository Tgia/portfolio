def xxx(*args):
    for xy in args:
        print(xy)
xxx('aaa','bbb','ccc','eee')
# Python program to illustrate   
# *kargs for variable number of keyword arguments 
  
def myFun(**kwargs):  
    for key, value in kwargs.items(): 
        print ("%s == %s" %(key, value)) 
  
# Driver code 
myFun(first ='Geeks', mid ='for', last='Geeks')  

