python = 30
anaconda = 40
rattlesnake = 15
 
if anaconda > python:
    print("We should take the anaconda.")
elif anaconda < python:
    print("We should not take the python.")
else:
    print("We can't decide.")