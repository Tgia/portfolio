x = "I like the way python works"
y = x.split()
 
def print_first_word(words):
    """Prints the first word after popping it off."""
    word = words.pop(0)
    print(word)
 
print(y)
print_first_word(y)
print(y)