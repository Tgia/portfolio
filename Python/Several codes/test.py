print("String plays and joining.")                       
print("What you want to do is {}.".format('join'))                       
print("." * 30)  # what'd that do?  Python trying to do something that others would not do                      
end1 = "S"                       
end2 = "t"                       
end3 = "r"                       
end4 = "i" 
end4_1 = "n"                      
end5 = "g"                       
end6 = "C"                       
end7 = "o"                       
end8 = "n"                       
end9 = "c"                       
end10 = "a"                       
end11 = "t"                       
               
print(end1 + end2 + end3 + end4 + end4_1 + end5, end=' ')                       
print(end6 + end7 + end8 + end9 + end10 + end11)