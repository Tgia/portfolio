#include <stdio.h>


struct div_t {
    int cociente;
    int resto;
    };

struct div_t division(int x, int y) {
       
    struct div_t res;
    int i = 0;
    while (y <= x) {
        x = x - y;
        i = i + 1;
    }                                    
       
    res.cociente = i;
    res.resto = x;
       
    return res;                 
}

int main() {
    int x = 0;
    int y = 1;
  
    printf("Inserte el dividendo:\n");
    fflush(stdin);
    scanf("%d", &x);
           
    printf("Inserte el divisor:\n");
    fflush(stdin);
    scanf("%d", &y);
   
    if (y>0) {
        struct div_t res = division(x, y);
        printf("El cociente es %d y el resto %d\n",res.cociente , res.resto);
    }
    else { 
        printf("error, el divisor no puede ser 0\n");
    } 

    return 0;
}
