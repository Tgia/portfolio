#include <stdio.h>


int suma_hasta(int N) {
    
    int n = 0;
    int r = 0;
    
    while (n<=N){
        r = n + r;
        n = n + 1;                 
    }
    return r;
}

int main() {
    
    int n = 0;
    int N;
            
    printf("ingrese el valor que desea calcular\n");
    fflush(stdin);
    scanf("%d", &N);
    
    if (0<N){
        n = N;
        printf ("La suma hasta %d es %d\n", n, suma_hasta(n));
    }                
    else{
        printf("¡El número debe ser mayor o igual a 0!\n");}
    return 0;
}
