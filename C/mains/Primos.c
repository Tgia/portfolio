#include <stdio.h>
#include <stdbool.h>
#include <assert.h>

int valor_abs(int x){   
    
    if (x>=0)
        x = x;
    else 
        x = -x;
    return x;
}






bool es_primo(int N){

    bool prim = true;
    int i = 2;
    
    if (N>1){
      while (i<N){ 
          prim = prim && !(N % i == 0);
          i++;
      }          
    }
    else
        prim = false;
    return prim;
}
      

struct cants {
    
    int num;
    int suc;
    int primo;
};

struct cants cuenta(int e[], int N ,int x){
    
    struct cants res;
    int i = 0, r1 = 0, r2 = 0, r3 = 0, y;     ;                                           
    y = x + 1;
                                          
  while(i<N){  
    if (x == e[i]){
      r1++;
    }
    else if (y == e[i]){
      r2++;
    }
                                              
                                                

    if (es_primo (e[i])){
      r3++;
    }
    i++;
  }

  res.num = r1;
  res.suc = r2;                                     
  res.primo = r3;
  
  return res;
}


int main (void)
{
int N, x, m , L, i = 0;               

  printf("ingrese el tamaño del arreglo\n");
  scanf("%d",&N);
	int e[N];    
                    

         

  while (i < N) {
    
    printf("Inserte el elemento de la posición %d del arreglo: ", i);
    scanf("%d", &e[i]);    
     
      if( e[i]<0){  
        e[i] = valor_abs(e[i]);}
        i++;}
    
    printf ("ingrese el numero que desea buscar\n"); 
    scanf ("%d",&x);
    printf("ingrese la suma esperada del numero y su sucesor\n");
    scanf("%d",&m);
    printf("ingrese la cantidad de primos esperada\n");
    scanf("%d",&L);
       
                   
    struct cants res = cuenta(e, N, x);
                    
                    
    assert(m == (res.suc + res.num));
    assert(L == res.primo);
    printf("ok\n");
         
          
         return 0;
}
