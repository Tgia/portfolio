#include <stdio.h>
#include<stdlib.h>
#include <stdbool.h>
#include<time.h>
#include "unistd.h"

 

int
colour_saver(unsigned char colour[], uint position, int colour_counter)
{
		if(colour_counter == 0){
			
			colour[colour_counter] = position;
			colour_counter++;

		}else{
			
			for(int i=0; i<colour_counter; i++){
				colour[colour_counter] = position;
			}
			colour_counter++;
		}
		return colour_counter;
}

void
print_srarray(uint length, uint s[]) //debugg
{
	for(uint i = 0; i<length; i++){
	printf("%d ", s[i]);
	if((i+1) % 10 == 0 ){
		printf("\n");
	}
}
printf("\n");
}

uint
obtain_count(uint length)
{
	uint result;
	if(length == 50){
		result = 10;
	}
	else if (length == 100){
		result = 30;
	}
	else{
		result = 50;
	}
	return result;
}

bool is_coloured(unsigned char colour[],int colour_counter,int actual_print_position){

	for(int i=0; i < colour_counter;i++){
		if(actual_print_position == colour[i]){
			return true;
		}
	}
	return false;
}

void
change(uint length, unsigned char asciiArray[], 
unsigned char c[], int position, int colour_counter){
	
	uint column = 0;
	
	for(uint i = 0; i<length; i++){
		
		if(i == 0){
			//imprimo 10 filas
			for(uint row = 0; row<10; row++){
				if(row == 0){
					printf("  ");
				}
				printf("%d ", row);
			}
			printf("\n");
		}	
		
		if(i == 0){
			printf("%d ", column);
			column++;
		}

		if( is_coloured(c,colour_counter,i) ){

				printf("\033[1;36m");
				printf("%c ", asciiArray[i]);
				printf("\033[0m");
		}
		else{
			printf("%c ", asciiArray[i]);
		}
		
		if((i+1) % 10 == 0 ){
			printf("\n");
			
			if(column < 5){
				printf("%d ", column);
				column++;
			}
		}
	}
	printf("\n");
}

int pos(int x, int y)
{
	int position = 10*x + y;
	return position;
}

bool
search(int position, uint minesArray[])
{
	bool boolean=false;
	if(minesArray[position] == 1){
		boolean = true;
	}
	else
	{
		boolean = false;
	}
	return boolean;
}

void
print_userarray(uint length, unsigned char u[]){
	uint row = 0;
	uint colum = 0;
	for(uint i = 0; i<length; i++){
		
		if(i == 0){
			for(colum = 0; colum<10; colum++){
				if(colum == 0){
					printf("  ");
				}
				printf("%d ", colum);
			}
		printf("\n");
		}	
		if(i == 0){
			printf("%d ", row);
			row++;
		}
	
		printf("%c ", u[i]);
		
		if((i+1) % 10 == 0 ){
			printf("\n");
			if(row < 5){
				printf("%d ", row);
				row++;
			}
		}
	}
	printf("\n");
}


void
init_level(uint length, uint minecount, uint minesArray[], unsigned char asciiArray[], unsigned char colour[])
{
	srand(time(NULL));
	uint randomnum;

	//inicializa el array booleano en 0
	for(uint i = 0; i<length; i++){
		minesArray[i] = 0;
	}

	//arreglo de ascii ###
	for(uint j = 0; j<length; j++){
		asciiArray[j] = 35;
	}
	//agregamos las minas
	for(uint k = 0; k<minecount; k++){
		randomnum = rand() % (length);
		minesArray[randomnum] = 1;
	}
	
	/*for(uint l = 0; l<(length-minecount); l++){
			colour[l] = 35;
	}*/
}


uint
getMines(int level_difficult)
{
	uint minecount;
	switch (level_difficult){
	
		case 1:
			minecount = 10;
			break;
		
		case 2:
			minecount = 15;
			break;

		case 3:
			minecount = 20;
			break;
		
		default:
			break;
	}

	return minecount;
}


int
print_level_difficult()
{
	int dificult;
	printf("Seleccione dificultad: \n");
	printf("presione 1 para fácil\n");
	printf("presione 2 para media\n");
	printf("presione 3 para difícil\n");
	fflush(stdin);
	scanf("%d", &dificult);
	if (dificult <= 0 || dificult >= 3){
			printf("El número ingresado no es vádido, por favor presione 1, 2 o 3 para seleccionar\n");
		}
	return dificult;
}

int 
main()
{
	int x;
	int y;
	
	int level_difficult = print_level_difficult();
	
	int position = 0;
	bool mine = false;
	system("clear");
	uint length = 50;
	int colour_counter = 0;//contador colores
	uint minecount = getMines(level_difficult);//cantidad de minas x dificultad
	
	uint minesArray[length];//array booleano
	unsigned char asciiArray[length];//array ascii #
	unsigned char colour[length-minecount];//array de colores

	init_level(length, minecount, minesArray, asciiArray, colour); //inicializa la matriz
	print_userarray(length, asciiArray);//printea el ascii
	print_srarray(length, minesArray); //debugg
	
	while(1){
		
		printf("ingrese coordenada\n");
		printf("Ejemplo: 2 1 representa la coordenada(2,1) de la forma (x,y)\n");
		fflush(stdin);
		scanf("%d %d", &x, &y);
		
		if ( (x > 5 || y > 9) || (x < 0 || y < 0) ){
			printf("\nla coordenada ingresada no es válida\n");
			printf("\n");
			print_userarray(length, asciiArray);//printea el ascii
			printf("\n");
			continue;
		}
		
		position = pos(x, y);
		printf("Posicion: %d\n",position);
		mine = search(position, minesArray);

		if(mine == true){
			printf("GAME OVER\n");
			break;
		}
		else{
			system("clear");
			colour_counter = colour_saver(colour, position, colour_counter);
			change(length, asciiArray, colour, position, colour_counter);
			continue;
		}
	}
}