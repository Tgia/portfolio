#include "types.h"
#include "stat.h"
#include "user.h"
#include "f.h"

void paint(int paleta[], unsigned int y, unsigned int x, unsigned int size)
{
    for(unsigned int column=y;column<size+y; column++){
      for(unsigned int row=x; row<size+x; row++){
        plotpixel(row,column,paleta[(column-y)*size+(row-x)]);
      }
    }
}
int
main(void)
{
  modeswitch(1);

  int j=0,i=0;
  
  for(j=0;j<320;++j){
    for(i=0; i<200; ++i){
      plotpixel(j,i,0);
    }
  }

  while(1){
    
    for(int i = 0; i<50; i++){   
      paint(img,0,0,20);
    }  
    paint(zero,0,0,20); 

    for(int i = 0; i<50; i++){   
      paint(img,50,50,20);
    } 
    paint(zero,50,50,20);

    for(int i = 0; i<50; i++){   
      paint(img,100,100,20);
    } 
    paint(zero,100,100,20);
  
    for(int i = 0; i<50; i++){   
      paint(img,150,150,20);
    }  
    paint(zero,150,150,20);
  
    for(int i = 0; i<50; i++){   
      paint(img,100,200,20);
    } 
    paint(zero,100,200,20);
  
    for(int i = 0; i<50; i++){   
      paint(img,150,250,20);
    } 
    paint(zero,150,250,20);
  
    for(int i = 0; i<50; i++){   
      paint(img,100,300,20);
    } 
    paint(zero,100,300,20);
  }
  exit();
}

