#include "types.h"
#include "stat.h"
#include "user.h"
#include "ball.h"


void
paint(int palet[], unsigned int y, unsigned int x, unsigned int size)
{
  for(unsigned int column=y;column<size+y; column++){
    for(unsigned int row=x; row<size+x; row++){
      plotpixel(row,column,palet[(column-y)*size+(row-x)]);
    }
 	}
}
void
paint_efect(unsigned int colour, unsigned int y, unsigned int x, unsigned int start_y, unsigned int start_x)
{
	for(unsigned int column=0+start_y; column<y; column++){
    for(unsigned int row=0+start_x; row<x; row++){
			plotpixel(row, column, colour);
		}
	}	
}

int
main(void)
{
	modeswitch(1);
  while(1){
    
    int j=0,i=0;
    paint_efect(63,200,320,0,0);
  
    paint(ash, 93, 50 , 17);
    for(int r=270; r>160; --r){
      paint(pikachu, 94, r, 16);
    }
    for(int r=50; r<145; r++){
      paint(ash, 93, r, 17);
    }
    paint_efect(0,200,64,0,0);

    for(i=64; i<320; ++i){
      for(j=160; j<200;++j){
        plotpixel(i,j,0);
      }
    }

    for(j=160;j>=0;--j){
      for(i=256; i<320; ++i){
        plotpixel(i,j,0);
      }
    }

    for(i=256; i>=64;--i){
      for(j=0;j<40;++j){
        plotpixel(i,j,0);
      }
    }

    for(j=40;j<160;++j){
      for(i=64;i<128;++i){
        plotpixel(i,j,0);
      }
    }

    for(i=128; i<256;++i){
      for(j=120;j<160;++j){
        plotpixel(i,j,0);
      }
    }

    for(j=120;j>=40;--j){
      for(i=192; i<256; ++i){
        plotpixel(i,j,0);
      }
    }

    for(i=192; i>=128;--i){
      for(j=80;j>=40;--j){
        plotpixel(i,j,0);
      }
    }
    
    paint_efect(0,120,192,80,128);
    paint_efect(63,160,320,40,0);

    paint(ashHD,110,105,30);

    paint(pikachuHD,70,205,20);

    int e = 110;
    for(int r=135; r<169;++r){
      paint(pokeball, --e, r, 14);}
    for(int r=169; r<190;++r){
      paint(pokeball, e, r, 14);
    }


    for(unsigned int i=0;i<35;i++){
      if (i>=0 && i<5){
        paint(pokeball,76,190,14);
      }
      if (i>=5 && i<10){
      paint(pokeball2,75,190,15);
      }
      if (i>=10 && i<25){//15
        if(i == 10){
        paint_efect(63,90,225,70,205);
        }
      paint(pokeball3,72,189,18);
      }
      if (i>=25 && i<30){//5
        if(i == 25){
          paint_efect(63,75,207,71,189);
        }
        paint(pokeball2,75,190,15);
      }
      if (i>=30 && i<35){//5
        paint(pokeball,76,190,14);
      }
    }
  }
  exit();
}
