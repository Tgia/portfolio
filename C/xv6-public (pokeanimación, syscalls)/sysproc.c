#include "types.h"
#include "x86.h"
#include "defs.h"
#include "date.h"
#include "param.h"
#include "memlayout.h"
#include "mmu.h"
#include "proc.h"
#include "vga.h"

int
sys_fork(void)
{
  return fork();
}

int
sys_exit(void)
{
  exit();
  return 0;  // not reached
}

int
sys_wait(void)
{
  return wait();
}

int
sys_kill(void)
{
  int pid;

  if(argint(0, &pid) < 0)
    return -1;
  return kill(pid);
}

int
sys_getpid(void)
{
  return myproc()->pid;
}

int
sys_sbrk(void)
{
  int addr;
  int n;

  if(argint(0, &n) < 0)
    return -1;
  addr = myproc()->sz;
  if(growproc(n) < 0)
    return -1;
  return addr;
}

int
sys_sleep(void)
{
  int n;
  uint ticks0;

  if(argint(0, &n) < 0)
    return -1;
  acquire(&tickslock);
  ticks0 = ticks;
  while(ticks - ticks0 < n){
    if(myproc()->killed){
      release(&tickslock);
      return -1;
    }
    sleep(&ticks, &tickslock);
  }
  release(&tickslock);
  return 0;
}

// return how many clock tick interrupts have occurred
// since start.
int
sys_uptime(void)
{
  uint xticks;

  acquire(&tickslock);
  xticks = ticks;
  release(&tickslock);
  return xticks;
}

int
sys_note(void)
{
  *(int *)P2V(0xB8F98) = 0x4354; //T
  *(int *)P2V(0xB8F9A) = 0x4367; //G
  *(int *)P2V(0xB8F9C) = 0x436c; //I
  *(int *)P2V(0xB8F9E) = 0x4361; //A
  return 0;
}

void
ac(unsigned char index, unsigned char value)
{
  inb(0x3DA);
  outb(0x3C0, index);
  outb(0x3C0, value);
}

void
misc(unsigned char value)
{
  outb(0x3C2, value);
}

void
seq(unsigned char index, unsigned char value)
{
  outb(0x3C4, index);
  outb(0x3C5, value);
}

void
gp(unsigned char index, unsigned char value)
{
  outb(0x3CE, index);
  outb(0x3CF, value);
}

void
crtc(unsigned char index, unsigned char value)
{
  outb(0x3D4, index);
  outb(0x3D5, value);
}

void
gmode()
{        
  uint i = 0, index = 0x00;
        
  misc(graphic_mode_320x200x256[i]);

  while(i<5){
    i++;
    seq(index, graphic_mode_320x200x256[i]);
    index++;
  }
  index = 0x00;

  while(i<30){
    i++;
    crtc(index, graphic_mode_320x200x256[i]);
    index++;
  }
  index = 0x00;
  
  while(i<39){
    i++;
    gp(index, graphic_mode_320x200x256[i]);
    index++;
  }
  index = 0x00;
  
  while(i<60){
    i++;
    ac(index, graphic_mode_320x200x256[i]);
    index++;
  }   

  inb(0x3DA);
  outb(0x3C0, 0x20);
}

void
tmode()
{      
  uint i = 0, index = 0x00;
        
  misc(text_mode_80x25[i]);

  while(i<5){
    i++;
    seq(index, text_mode_80x25[i]);
    index++;
  }
  index = 0x00;

  while(i<30){
    i++;
    crtc(index, text_mode_80x25[i]);
    index++;
  }
  index = 0x00;
  
  while(i<39){
    i++;
    gp(index, text_mode_80x25[i]);
    index++;
  }
  index = 0x00;

  inb(0x3DA);
  
  while(i<60){
    ac(index, text_mode_80x25[i]);
    index++;
    i++;
  }   
 
  inb(0x3DA);
  outb(0x3C0, 0x20);
}

void
mode_switch(int mode)
{
    if(mode == 0){
        tmode();
    }
    else if(mode == 1){
        gmode();
    }
}

int
sys_modeswitch(void)
{
  int mode;
  argint(0,&mode);
  mode_switch(mode);
  return 0;
}

void
plot_pixel(int x, int y, int colour)
{
  uchar *VGA = (uchar*)P2V(0xA0000);
  int position = 320*y + x;
  VGA[position] = colour;
}

int
sys_plotpixel(void)
{
  int x,y,colour;
  argint(0,&x);
  argint(1,&y);
  argint(2,&colour);
  plot_pixel(x,y,colour);
  return 0;
}


