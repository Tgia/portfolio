#include "types.h"
#include "stat.h"
#include "user.h"
#include "nero.h"

void paint(int paleta[], unsigned int y, unsigned int x, unsigned int size)
{
  for(unsigned int row=x; row<size+x; row++){
    for(unsigned int column=y;column<size+y; column++){
      plotpixel(row,column,paleta[(column-y)*size+(row-x)]);
    }
  }
}

int
main(void)
{
  modeswitch(1);

  int j=0,i=0;
  for(i=0; i<200; ++i){
    for(j=0;j<320;++j){
      plotpixel(j,i,0);
    }
  }

  
  paint(nero, 50, 110, 100);
  
  exit();
}

