#include "memlayout.h"
#include "types.h"
#include "stat.h"
#include "user.h"

int
main(void)
{
  *(int *)P2V(0xB87C6) = 0x4353; //S
  *(int *)P2V(0xB87C8) = 0x434F; //O
  *(int *)P2V(0xB87CA) = 0x4332; //2
  *(int *)P2V(0xB87CC) = 0x4330; //0
  *(int *)P2V(0xB87CE) = 0x4331; //1
  *(int *)P2V(0xB87D0) = 0x4339; //9
}