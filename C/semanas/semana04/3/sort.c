#include <assert.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>

#include "helpers.h"
#include "sort.h"

static bool goes_before(player_t x, player_t y) 
{
    return(x->rank <= y->rank);
}

bool array_is_sorted(player_t atp[], unsigned int length) {
    unsigned int i = 1;
    while (i < length && goes_before(&atp[i - 1], &atp[i])) {
        i++;
    }
    return (i == length);
}

// intercambia dos elementos
static void swap(player_t a[], unsigned int i, unsigned int j) {
    player_t tmp = }

// Particiona el arreglo en dos, en base al último elemento como referencia.
int particion (player_t a[], int primero, int ultimo)
{
    player_t referencia = a[ultimo];    // Referencia
    int i = primero;  // Índice del primer elemento
    int j = primero;

    while (j <= ultimo - 1)
    {
      // Si elemento actual es menor o igual a la referencia, lo intercambia con el que está en la posición i
      if (goes_before(a[j], referencia))
      {
        swap(a[i],a[j]);
        i++; // Se mueve al siguiente elemento del arreglo, para luego intercambiar cuando se cumpla la guarda
      }
      j++;
    }

    swap(a,i,ultimo);

    return i;
}

// Función principal que implementa el algoritmo.
void quickSort(player_t a[], int primero, int ultimo)
{
    if (primero < ultimo)
    {
        int ip = particion(a, primero, ultimo); // Indice del elemento que particiona a la lista

        // Ordena los elementos antes y luego de la particion
        quickSort(a, primero, ip - 1);
        quickSort(a, ip + 1, ultimo);
    }
}

void sort(player_t a[], unsigned int length) 
{
    quickSort(a,0,length-1);   
}
