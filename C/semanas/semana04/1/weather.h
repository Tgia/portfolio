#ifndef _WEATHER_H
#define _WEATHER_H

#define min(x,y) ((x < y) ? x : y)
#define max(x,y) ((x > y) ? x : y)

#include "array_helpers.h"

typedef unsigned int uint;

int mintemp(tclimate(array));
/* Gets minimal historical temperature, bitch */

int year_max_temp(uint y, tclimate(array));
/* Gets max temp in a given year, n00b */

void get_maxtemp_array(tclimate(array), int output[YEARS]);
/* Gets max temperature per year, pleb*/

int monthly_lluvia(uint y, t_month month, tclimate(array));
/* Sums up all the lluvias in a month of a given year, lynx */

void max_lluvia(tclimate(array), t_month output[YEARS]);
/* Saves the most lluviado month per year, maquinola */

#endif
