#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include "weather.h"
#include "array_helpers.h"

 
 
int mintemp(tclimate(array))
{   
    int minimo = array[0][0][0][temp_m]; 
    for (unsigned int year = 0; year < YEARS; year++) 
    {   
        for (t_month month = january; month <= december; month++)
        {   
            for (unsigned int day = 0; day < DAYS; day++) 
            {  
                minimo = min(minimo,array[year][month][day][temp_m]);
            }
        }
    }
    return minimo;
}

int year_max_temp(uint y, tclimate(array))
{
    int maximo = array[y][0][0][temp_M];
    
    for (t_month month = january; month <= december; month++)
    {
        for (uint day = 0; day < DAYS; day++)
        {
            maximo = max(maximo, array[y][month][day][temp_M]);
        }
    }
    return maximo ;

}           


void get_maxtemp_array(tclimate(array), int output[YEARS])
{
    for (uint yrs = 0; yrs<YEARS; yrs++)
    {
        output[yrs] = year_max_temp(yrs,array); 
    }
}

int monthly_lluvia(uint y, t_month month, tclimate(array))
{
    int amount_precip = 0;
    
    for (uint day = 0; day < DAYS; day++)
    {
        amount_precip = amount_precip + array[y][month][day][precip];
    }

    return amount_precip;
}

void max_lluvia(tclimate(array), t_month output[YEARS])
{
    for (unsigned int year = 0; year < YEARS; year++) 
    {   
        int max_lluvia_month = 0;
        max_lluvia_month = monthly_lluvia(year, january, array);
        output[year] = january;
    
        for (t_month month = february; month <= december; month++)
        {
            if (max_lluvia_month < monthly_lluvia(year, month, array))
            {
                max_lluvia_month = monthly_lluvia(year, month, array);
                output[year] = month;
            }
        }
    }
}
