/* First, the standard lib includes, alphabetically ordered */
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

/* Then, this project's includes, alphabetically ordered */
#include "sorted_set.h"
int MAX_SIZE = 10;

int main()
{
    sorted_set s = create_sorted_set();
    sorted_set s2 = create_sorted_set();

    int ordenados[MAX_SIZE];
    int inversos[MAX_SIZE];
    int repetidos[MAX_SIZE];

    for (t_elem i = 0; i < MAX_SIZE; i++)
    {
        ordenados[i] = i*3;
        inversos[i] = MAX_SIZE - i;
        repetidos[i] = 3;
    }

    for (t_elem i = 0; i < MAX_SIZE; i++)
    {
        add(s, ordenados[i]);
        printf("Agregado el elemento %d al set ordenado\n", ordenados[i]);
    }

    dump_sorted_set(s);

    for (t_elem i = 0; i < MAX_SIZE; i++)
    {
        add(s2, inversos[i]);
        printf("Agregado el elemento %d al set inverso\n", inversos[i]);
    }

    dump_sorted_set(s2);

    for (t_elem i = 0; i < MAX_SIZE; i++)
    {
        add(s2, repetidos[i]);
        printf("Agregado el elemento %d al set inverso\n", repetidos[i]);
    }

    dump_sorted_set(s2);

    return (EXIT_SUCCESS);
}
