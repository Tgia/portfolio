#include <stdlib.h>
#include <stdio.h>
#include "sorted_set.h"

typedef struct _node_t * node_t;

typedef struct _node_t
{
    t_elem value;
    node_t next;
} _node_t;

typedef struct _sorted_set
{
    node_t first;
} _sorted_set;

sorted_set create_sorted_set()
{
    sorted_set s;
    s = (sorted_set) malloc(sizeof(_sorted_set));
    s->first = NULL;
    return s;
}

void add(sorted_set s, t_elem e)
{
    /*
        El set está ordenado
    */

    // Caso especial para el comienzo
    if (s->first == NULL || (s->first)->value > e)
    {
        node_t nodito = (node_t)malloc(sizeof(_node_t));
        nodito->value = e;
        nodito->next = s->first;
        s->first = nodito;
    }
    else if ((s->first)->value < e)
    {
        node_t actual = s->first;

        while (actual->next != NULL && actual->next->value < e)
            actual = actual->next;
        //Pre : actual->next == NULL || actual->next->value >= e
        if (actual->next == NULL ||
            (actual->next != NULL && actual->next->value > e))
        {
            node_t nodito = (node_t)malloc(sizeof(_node_t));
            nodito->value = e;
            nodito->next = actual->next;
            actual->next = nodito;
        }
    }
}

//Pre = !is_empty(s)
void rm(sorted_set s, t_elem e)
{
	node_t p = s->first;
	//special case head //
	if(p->value == e)
	{
		s->first = p->next;
		free(p);
	}
	else if (p->value < e)
	{
		while (p->next !=NULL && p->next->value < e)
			p = p->next;
		// Pos p->next == NULL || p->next->elem >= e //
		if(p->next != NULL && p->next->value == e)
		{
			node_t aux = p->next;
			p->next = aux->next;
			free(aux);
		}
	}
}

int size(sorted_set s)
{
    int tam = 0;
    node_t actual = s->first;

    while (actual != NULL)
    {
        actual = actual->next;
        tam++;
    }
    return tam;
}

bool is_empty(sorted_set s)
{
	return(s->first);
}

bool contains(sorted_set s, t_elem e)
{
    bool existe = false;
    node_t actual = s->first;

    while (!existe && actual != NULL)
    {
        existe = actual->value == e;
        actual = actual->next;
    }

    return existe;
}

t_elem first(sorted_set s)
{
	return(s->first->value);
}

// Pre: !is_empty(s)
t_elem last(sorted_set s)
{
    node_t actual = s->first;

    while (actual->next != NULL)
        actual = actual->next;

    return (actual->value);
}

void dump_sorted_set(sorted_set s)
{
    node_t actual = s->first;

    printf("Set: ");
    while (actual != NULL)
        {
            printf("%d ", actual->value);
            actual = actual->next;
        }
    printf("\n");
}

void destroy(sorted_set s)
{
	while(s->first != NULL)
	{
		t_elem e = first(s);
		rm(s,e);
	}
	free(s);
	s = NULL;
}
