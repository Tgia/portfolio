#include <stdbool.h>
#include <stdlib.h>

#include "queue.h"

/* node_t the type of pointers to nodes
*/
typedef struct _node_t * node_t;

/* _node_t the type of nodes
*/
struct _node_t {
    queue_elem elem;
    node_t next;
} _node_t;

/* define this way if you will use circular linked lists
*/
typedef struct _queue_t {
    node_t the_last;
} _queue_t;

/****************************/
/*            OR            */
/****************************/

/* define this way if you will linked lists with pointer to the first and last nodes
*/
/*typedef struct _queue_t {
    node_t start;
    node_t end;
} _queue_t;*/

queue_t create_queue() 
{
	queue_t q = (queue_t)malloc(sizeof(_queue_t));
	q->the_last=NULL; 
	return q; 
}

void empty_queue(queue_t q) 
{
	while(!is_empty_queue(q))
	{
		dequeue(q);
	}	
}

/* pre: !is_full_queue(q) */
void enqueue(queue_t q, queue_elem e) 
{
	node_t nodito = (node_t)malloc(sizeof(_node_t));
	nodito->elem = e; 

	if(is_empty_queue(q))	// e is first element
	{
		nodito->next = nodito;
	}
	else // q already has elements 
	{
		nodito->next = q->the_last->next;
		q->the_last->next = nodito;
	}
	q->the_last = nodito;
}

/* pre: !is_empty_queue(q) */
queue_elem first(queue_t q) 
{
	return(q->the_last->next->elem);  //WHY!!!!!?!?!?!?!?!?!?
}

/* pre: !is_empty_queue(q) */
void dequeue(queue_t q) 
{
	if((q->the_last) == (q->the_last->next))
	{
		free(q->the_last);
		q->the_last = NULL; 
	}
	else
	{
		node_t aux = q->the_last->next;
		q->the_last->next = aux->next;
		free(aux); 
	}
}

bool is_empty_queue(queue_t q) 
{
	return(!(q->the_last)); 
}

bool is_full_queue(queue_t q) 
{
	return(q==NULL && q!=NULL); 
}

void destroy_queue(queue_t q) 
{
	empty_queue(q);
	free(q);
	q = NULL;
}

