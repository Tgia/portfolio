#include <assert.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>

#include "array_helpers.h"
#include "sort.h"


static void swap(int a[], unsigned int i, unsigned int j) {
    int tmp = a[i];
    a[i] = a[j];
    a[j] = tmp;
}

bool array_is_sorted(int array[], unsigned int length, bool order) 
{
	bool res = true;

	if (length > 0)
	{
		if (order) // Mayor a menor
		{
			for(unsigned int i = 0; (i< length-1)&&res; i++)
			{
				res = res && (array[i]>=array[i+1]);		
			}
		}
		else // Menor a mayor
		{
			for(unsigned int i = 0; (i< length-1)&&res; i++)
			{
				res = res && (array[i]<=array[i+1]);		
			}
		}
	}
	return res;	
}

static unsigned int min_pos_from(int a[], unsigned int i, unsigned int length) {
    unsigned int min_pos = i;
    for (unsigned int j = i + 1; j < length; j++) {
        if (a[j] < a[min_pos]) {
            min_pos = j;
        }
    }
    return (min_pos);
}

static unsigned int max_pos_from(int a[], unsigned int i, unsigned int length) {
    unsigned int max_pos = i;
    for (unsigned int j = i + 1; j < length; j++) {
        if (a[j] > a[max_pos]) {
            max_pos = j;
        }
    }
    return (max_pos);
}

void selection_sort(int a[], unsigned int length, bool order) {
	if(order){ // Mayor a menor
		for(unsigned int i = 0 ; i<length; i++){
			unsigned max_pos = max_pos_from(a, i, length);
			swap(a, i, max_pos);	
		}
	}
	else{ // Menor a mayor
		for (unsigned int i = 0; i < length; i++) {
        	unsigned int min_pos = min_pos_from(a, i, length);
        	swap(a, i, min_pos);
    	}
	}
}


