#include "TADNatural.h"
#include "TADBooleano.h"
#include <stdio.h>

Natural sucesor(Natural n){
	n.value++;
	return n;
}

Booleano es_cero(Natural n){
	Booleano r;
	if(n.value == cero){
		r.value = verdadero;
	}
	else{
		r.value= falso;
	}
	return r;
}

Natural predecesor(Natural n){
	if (not(es_cero(n)).value){
		n.value--;		
	}	
	return n;
}


Booleano menor_igual(Natural n, Natural m){
	Booleano r;
	
	if (n.value <= m.value){
		r.value = verdadero;
	}
	else{
		r.value= falso;
	}
	return r;
}

Natural mas(Natural n, Natural m){
	Natural r;
	r.value = n.value + m.value;
	return r;
}

Natural menos(Natural n, Natural m){
	Natural r;
	r.value = n.value - m.value;
	return r;
}

Natural por(Natural n, Natural m){
	Natural r;
	r.value = n.value * m.value;
	return r;
}

Booleano es_multiplode(Natural n, Natural m){
	
	if(n.value % m.value == 0){
		return true();
	}	
	else{
		return false();
	}
}
		 	
