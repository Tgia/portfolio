#ifndef _BOOLEANO_H
#define _BOOLEANO_H


typedef unsigned int uint;
typedef struct _Booleano {
	uint value;
} Booleano;

#define verdadero 1
#define falso 0

Booleano true ();
/*Logic constructor*/

Booleano false ();
/*logic constructor*/

Booleano and (Booleano p, Booleano q);
/*logic and*/

Booleano or (Booleano p, Booleano q);
/*logic or*/

Booleano not (Booleano p);
/*negation*/

uint eval (Booleano p);
/*logic evaluation*/

#endif
