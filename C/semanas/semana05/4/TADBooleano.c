#include "TADBooleano.h"

Booleano true (){
	Booleano r;
	r.value = verdadero;
	return r;
}

Booleano false (){
	Booleano r;
	r.value = falso;
	return r;
}

Booleano and (Booleano p, Booleano q){
	Booleano r;
	if(p.value == verdadero && q.value == verdadero){
		r.value = verdadero;
	}		
	else{
		r.value = falso;
	}	
	return (r);
}

Booleano or (Booleano p, Booleano q){
	Booleano r;
	if(p.value == falso && q.value == falso){
		r.value = falso;
	}	
	else{
		r.value = verdadero;
	}
	return r;
}

Booleano not (Booleano p){
	if(p.value == verdadero){
		p.value = falso;
	}
	else{
		p.value = verdadero;
	}
	return p;
}


uint eval (Booleano p){
	if (p.value == verdadero){
		return verdadero;
	}
	else{
		return falso;
	}
}
