#ifndef _TADNATURAL_H
#define _TADNATURAL_H
#include "TADBooleano.h"

typedef struct _Natural 
{
	uint value;
} Natural;

#define cero 0

Natural sucesor(Natural n);
/*return sucesor*/

Natural predecesor(Natural n);
/*return predecesor*/

Booleano es_cero(Natural n); 
/*check the fkn number*/

Booleano menor_igual(Natural n, Natural m);
/*x2*/

Natural mas(Natural n, Natural m);
/*natural addition*/

Natural menos(Natural n, Natural m);
/*natural substraction*/

Natural por(Natural n, Natural m);
/*natural multiplication*/

Booleano es_multiplode(Natural n, Natural m);
/*cheating*/

#endif
