#ifndef _COUNTER_H
#define _COUNTER_H

#include <stdbool.h>

typedef unsigned int counter;

counter init(counter c);
/*check if the counter is equal to zero*/
	
	

counter  inc(counter c);
/*increase the counter*/
	


counter  dec(counter c);
/*decrease the counter*/
	

bool is_init(counter c);
/* just a simple check (if the value of the counter is the initial value)*/
#endif
