#include <stdbool.h>

#include "counter.h"

counter init(counter c) {
	if(!(c == 0)){
		c = 0;
	}
	return c;
}

counter inc(counter c) {
	c++;
	return c;
}

counter dec(counter c) {
	c--;
	return c;
}

bool is_init(counter c) {

    if (c == 0){ 
		return true;
	}
	else{
		return false;
	}

}

