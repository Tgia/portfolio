#include <stdbool.h>

#include "counter.h"

struct _counter {
    unsigned int count;
};


void init(counter c) {
	if ( c->count != 0){
		c->count = 0;
	}
}

void inc(counter c) {
	c->count++;
}

void dec(counter c) {
	c->count--;
}

bool is_init(counter c) {
	return (c == 0);
}	

