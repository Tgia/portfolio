#include <stdbool.h>

#include "counter.h"

void init(counter *pc) {
	if (*pc != 0){
		*pc = 0;
	}
}

void inc(counter *pc) {
	*pc = *pc+1;
}

void dec(counter *pc) {
	*pc = *pc-1;
}

bool is_init(counter c) {
	if(c != 0){
		return false;
	}
	else{
		return true;
	}
}
