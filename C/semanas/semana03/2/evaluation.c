#include "prop.h"
#include "evaluation.h"

typedef unsigned int uint;

bool eval(prop_t prop, bool line[]) {
/* evaluates the proposition in the given line of the truth table
 * assumes array line has the values for all the propositional letters
 * in the proposition
 */
    bool res;
    if (is_prop_true(prop)) {
        res = true;
    } else if (is_prop_false(prop)) {
        res = false;
    } else if (is_prop_var(prop)) {
        res = line[get_var(prop)];
    } else if (is_prop_not(prop)) {
        res = !eval(get_sub_prop(prop),line);
    } else if (is_prop_and(prop)) {
        res = eval(get_left_prop(prop),line) && eval(get_right_prop(prop),line);
    } else if (is_prop_or(prop)) {
        res = eval(get_left_prop(prop),line) || eval(get_right_prop(prop),line);
    } else if (is_prop_then(prop)) {
        res = !eval(get_left_prop(prop),line) || eval(get_right_prop(prop),line);
    } else if (is_prop_iff(prop)) {
        res = eval(get_left_prop(prop),line) == eval(get_right_prop(prop),line);
    } 
    return (res);
}

static void next_line(bool line[], unsigned int length) 
{
	bool carry = true;
	uint i = length; 
	while(i>0 && carry)
	{
		if(line[i-1] && carry)
		{
			line[i-1] = false;		
		}
		else if(!(line[i-1]) && carry )
		{
			line[i-1] = true; 
			carry = false;		
		}
		i--; 		
	}
}


static void mk_fst_line(bool line[], unsigned int length) 
{
    for(uint i = 0; i<length; i++)
    {
		line[i]=false;
	} 
    
}

static bool last_line(bool line[], unsigned int length)
{
    uint i = 0;
	bool res = true;

    while ((i < length) && res)
    {
		res = res && line[i];
    	i++;
    }

	return res;
}


bool is_tautology(prop_t prop)
{	
	
	uint length = get_max_var(prop);
	
	bool line[length];
	
	mk_fst_line(line,length);
	bool res = eval(prop,line);
	if(res)
	{	
		while(!last_line(line,length) && res)
		{
			next_line(line,length);	
			res = res && eval(prop,line);	
		}
			
	}	
	return res; 
}


